({
	getCommunityUserCustomName : function(component) {
		console.log('spHelloUserMsgHelper -> getCommunityUserCustomName (v1)');
		var self = this;
		var action = component.get("c.getCommunityUserCustomName");
        action.setCallback(this, function(response) {
        	if (self.isValidServerResponse(response, true)) {
        		var result = response.getReturnValue();
        		self.getCommunityUserCustomNameCallback(component, result);
        	}
        });
        $A.enqueueAction(action);

	},

	getCommunityUserCustomNameCallback : function(component, communityUserCustomName) {
		console.log('getCommunityUserCustomNameCallback ->  communityUserCustomName: ' + communityUserCustomName);
		component.set("v.communityUserCustomName", communityUserCustomName);
		component.set("v.loaded", true);
	},

	isValidServerResponse : function(response, required) {
		var state = response.getState();
		if (state === "SUCCESS") {
			if (required) {
				var result = response.getReturnValue();
        		if (!result) {
        			console.log("Unknown error, server result is null");
        			return false;
        		}
			}
			return true;
		}
		//else...
		var errors = response.getError();
		if (errors) {
			if (errors[0] && errors[0].message) {
				console.log("Error message: " + errors[0].message);
			}
		} else {
			console.log("Unknown error");
			alert('An internal server error has occurred');
		}
		return false;
	}
})