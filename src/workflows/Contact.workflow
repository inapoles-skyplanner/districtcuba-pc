<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>dcEmailAlertPasaporteVence</fullName>
        <description>dcEmailAlertPasaporteVence</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Email_Templates/dcEmailTemplate_VencePasaporte</template>
    </alerts>
    <alerts>
        <fullName>dcEmailAlertProrrogaVence</fullName>
        <description>dcEmailAlertProrrogaVence</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Email_Templates/dcEmailTemplate_VenceProrroga</template>
    </alerts>
</Workflow>
