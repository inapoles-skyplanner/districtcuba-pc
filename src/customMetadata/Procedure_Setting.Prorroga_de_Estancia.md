<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Prorroga de Estancia</label>
    <protected>false</protected>
    <values>
        <field>Agency_Commission__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ConsulatePayment__c</field>
        <value xsi:type="xsd:double">190.0</value>
    </values>
    <values>
        <field>Distributor_Cost__c</field>
        <value xsi:type="xsd:double">190.0</value>
    </values>
    <values>
        <field>FedexServiceType__c</field>
        <value xsi:type="xsd:string">FedEx Standard Overnight</value>
    </values>
    <values>
        <field>FirstCheckAmount__c</field>
        <value xsi:type="xsd:double">40.0</value>
    </values>
    <values>
        <field>GrantCheckAmount__c</field>
        <value xsi:type="xsd:double">150.0</value>
    </values>
    <values>
        <field>IndividualPayment__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Number_of_Packages__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Package_Type__c</field>
        <value xsi:type="xsd:string">FedEx Envelope</value>
    </values>
    <values>
        <field>Package_Weight__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Procedure_Type__c</field>
        <value xsi:type="xsd:string">Prorroga de Estancia</value>
    </values>
    <values>
        <field>Sales_Price__c</field>
        <value xsi:type="xsd:double">250.0</value>
    </values>
    <values>
        <field>SecondCheckAmount__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Subtype__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
