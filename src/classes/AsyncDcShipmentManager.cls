/**
 * @File Name          : AsyncDcShipmentManager.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 24/7/2020 12:39:12 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/7/2020   Silvia Velazquez     Initial Version
**/
public with sharing class AsyncDcShipmentManager {
    AsyncDcFedexServiceClient.ProcessShipmentReplyFuture reply;
    public dcFedexServiceClient.ProcessShipmentReply result {get;set;}
    //public String result {get;set;} //trackingNumber

    // Action method
    public Continuation startRequest(Map<String,Object> params) {    
        Integer TIMEOUT_INT_SECS = 60;  
        Continuation cont = new Continuation(TIMEOUT_INT_SECS);
        cont.continuationMethod = 'processResponse';

        dcFedexServiceClient.WebAuthenticationDetail webAuth = new dcFedexServiceClient.WebAuthenticationDetail();
        webAuth.setCredentials(params);

        dcFedexServiceClient.ClientDetail ClientDetail = new dcFedexServiceClient.ClientDetail();
        ClientDetail.setDetails(params);

        dcFedexServiceClient.TransactionDetail TransactionDetail = new dcFedexServiceClient.TransactionDetail();
        TransactionDetail.setDetails(params);
        
        dcFedexServiceClient.VersionId Version = new dcFedexServiceClient.VersionId();
        dcFedexServiceClient.RequestedShipment RequestedShipment = new dcFedexServiceClient.RequestedShipment();
        RequestedShipment.AddShipmentDetails(params); //Package Detail, Service Detail, shipment Date
        RequestedShipment.CreateParties(params); //Create the Shipper and Recipient inside the RequestedShipment
        RequestedShipment.AddLabelSpecification(params);
        RequestedShipment.PackageCount = ((Decimal)params.get('PackageCount')).intValue();
        RequestedShipment.AddLineItem(params);
        
        //Email Notification as a Special Service in the Request.
        String mailServiceAddr = (String)params.get('emailServiceAddr');
        if(String.isNotBlank(mailServiceAddr)){
            Map<String,Object> notifications = new Map<String,Object>();
            notifications.put('role1','SHIPPER');
            notifications.put('email1',mailServiceAddr);
            notifications.put('name1','Email Service');

            notifications.put('role2','SHIPPER');
            notifications.put('email2','svelazquez@theskyplanner.com'); //**WARNING** This value must be read from a Custom Setting
            notifications.put('name2','Silvia Velazquez'); //**WARNING** This value must be read from a Custom Setting

            notifications.put('count',2);
            /* if(String.isNotBlank(recipEMailAddress)){
                notifications.put('count',3);
                notifications.put('role3','RECIPIENT');
                notifications.put('email3',recipEMailAddress);
                notifications.put('name3',recipPersonName); 
            }
            else{
                notifications.put('count',2);
            } */

            RequestedShipment.addSpecialNotificationService(notifications);
        }
        
        AsyncDcFedexServiceClient.AsyncShipServicePort shipService = new AsyncDcFedexServiceClient.AsyncShipServicePort();
        reply = shipService.beginProcessShipment(cont, webAuth, ClientDetail, TransactionDetail, Version, RequestedShipment);   
        return cont;   
    }    
    
    // Callback method
    public Object processResponse() {    
       result = reply.getValue();
       // Return null to re-render the original Visualforce page
       return null; 
    }
}