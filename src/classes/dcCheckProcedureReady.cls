/**
 * @File Name          : dcCheckProcedureReady.cls
 * @Description        : Class with invocable method to check if required fields have values
 * @Author             : Elizabeth Betancourt Herrera
 * @Group              : 
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 17/8/2020 2:39:43 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/8/2020   Elizabeth Betancourt Herrera     Initial Version
**/
public with sharing class dcCheckProcedureReady {

    @InvocableMethod(label='Check Required' description='Returns a string of required empty fields separated by commas. ')
    public static List<String> RequiredEmptyFields(List<String> idProcedures) {
        
            List<String> fields= new List<String>{''};
            /*   find the list of requiered fields for the corresponding record type  */
            if (idProcedures.size() > 0){
                  string idProcedure = idProcedures[0];
                  List<Procedure__c> procList = [SELECT RecordType.Name FROM Procedure__c WHERE Id= :idProcedure];
                  Map<String, String> fieldMap = getMapFields(procList[0].RecordType.Name); 
                   
                  String fieldStr = getFieldStr(fieldMap.values());
                  System.debug(fieldStr);
                  if ( String.isEmpty(fieldStr) ) return fields;
                      
                  String query =  'SELECT ' + fieldStr  + ' FROM Procedure__c WHERE Id=\'' + idProcedure +'\'';
                  System.debug(query);
                
                  procList = database.query(query);
                  
                  Map<string, Object> dvMapObject = procList[0].getPopulatedFieldsAsMap();
                  
                  String emptyFieldsStr = '';
                  String field; 
                  for (string label: fieldMap.keySet()){
                      field = fieldMap.get(label);
                      if (!dvMapObject.containsKey(field) || String.isEmpty(String.valueOf(dvMapObject.get(field))) ){
                          emptyFieldsStr = (String.isEmpty(emptyFieldsStr) ? label : emptyFieldsStr + ', ' + label);
                      }
                  }
                  fields[0] = emptyFieldsStr;
                  System.debug(emptyFieldsStr);
                  //guardar resultado
                  if (emptyFieldsStr.length() > 255) emptyFieldsStr = emptyFieldsStr.substring(0, 254);
                  procList[0].EmptyRequiredValues__c = emptyFieldsStr;
                  update procList[0];
           }            
           System.debug(fields);
           return fields;
    }
    
    private static String getFieldStr (List<String> strList){
        String fieldStr = '';
        for(String str: strList){
            fieldStr = ( String.isEmpty(fieldStr) ? str : fieldStr + ', '+ str);
        }
        return fieldStr;
    }
    
    private static Map<String, String> getMapFields (String recordTypeName){
        Map<String,String> fieldMap = new Map<String, String>();
        List<FieldsRequiredToReadyProcedure__mdt> mdtList = [SELECT Label, ProcedureFieldApiName__c 
                                                          FROM FieldsRequiredToReadyProcedure__mdt 
                                                          WHERE ProcedureRecordType__c= :recordTypeName ORDER BY Id];
        
        for (FieldsRequiredToReadyProcedure__mdt item: mdtList){
            fieldMap.put(item.Label, item.ProcedureFieldApiName__c);
        }
        return fieldMap;
    }
}