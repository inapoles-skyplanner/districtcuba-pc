/**
 * @File Name          : dcSearchUtilityTest.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 31/7/2020 2:23:15 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcSearchUtilityTest {

    public static dcSearchContact initSearch(){
        Map<String,String> params = TestDataFactory.getTramiteParams(true, true, true);
        dcSearchContact search = new dcSearchContact();
        search.birthdate = Date.Today().addYears(-30);
        search.firstName = 'Antonio';
        search.middleName = 'Isaac';
        search.lastName = 'Hernandez';
        search.secondLastName = 'Vargas';
        return search;
    }
 
    @isTest
    static void countTest(){
        Test.startTest();
        dcSearchContact searchPattern = initSearch();
        dcSearchContactResult results = dcSearchUtility.count(searchPattern);
        Test.stopTest();
        System.assert(results != null);
    }

    @isTest
    static void searchTest(){
        Test.startTest();
        dcSearchContact searchPattern = initSearch();
        dcSearchContactResult results = dcSearchUtility.search(searchPattern);
        Test.stopTest();
        System.assert(results != null);       
    }

    @isTest
    static void resetTest(){
        Test.startTest();
        dcSearchContact searchPattern = new dcSearchContact();
        dcSearchContactResult results = dcSearchUtility.reset(searchPattern);
        Test.stopTest();
        System.assert(results != null);   
    }

    @isTest
    static void updateProcedureTest(){
        Test.startTest();
        Map<String,String> params = TestDataFactory.getTramiteParams(true, true, true);
        Boolean result = dcSearchUtility.updateProcedure(params.get('contactId'), params.get('tramiteId'), 'K862199', Date.Today().addYears(2));
        Test.stopTest();
        System.assert(result);
    }
}