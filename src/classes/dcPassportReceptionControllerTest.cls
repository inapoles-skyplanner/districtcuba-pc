/**
 * @File Name          : dcPassportReceptionControllerTest.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 3/8/2020 11:10:35 a. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    27/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcPassportReceptionControllerTest {

    public static dcSearchContact initSearch(){
        String value = initSearchContact(null);
        dcSearchContact search = dcPassportReceptionController.initData(value);
        return search;
    }
    
    public static String initSearchContact(dcSearchContact pParams){
        dcSearchContact search;
        if (pParams == null) {
            search = new dcSearchContact();
            search.firstName = 'Ana';
            search.lastName = 'Vega';
            search.secondLastName = 'Rojas';
            search.birthdate = Date.today();
            search.pageSize = 10;
            search.offset = 0;
            search.sortBy = 'Name, CreatedDate ';
            search.isDescending = true;
        } else {
            search = new dcSearchContact(pParams);
        }    
        return JSON.Serialize(search);
    }

    static testMethod void decodeJsonTest() {
        Test.startTest();
        String search = initSearchContact(null);
        dcSearchContact searchContact = dcPassportReceptionController.decodeJson(search);
        System.assert (searchContact != null);
        Test.stopTest();
    }
    
    static testMethod void initDataTest() {
        Test.startTest();
        String search = initSearchContact(null);
        dcSearchContact searchContact = dcPassportReceptionController.initData(search);
        System.assert (searchContact != null && searchContact.firstName.length() > 0);
        Test.stopTest();
    }
    
    static testMethod void counTest() {
        Test.startTest(); 
        String search = initSearchContact(null);
        dcSearchContactResult result = dcPassportReceptionController.count(search);
        System.assert (result != null);
        Test.stopTest();
    }
     
    static testMethod void searchTest() {
        Test.startTest();
        String search = initSearchContact(null);
        dcSearchContactResult result = dcPassportReceptionController.search(search);
        System.assert (result != null && result.contactResults != null);
        Test.stopTest();
    }
    
    static testMethod void resetTest() {
        Test.startTest();
        String search = initSearchContact(null);
        dcSearchContactResult result = dcPassportReceptionController.reset(search);
        System.assert (result.searchParams != null);
        Test.stopTest();
    }

    static testMethod void updateProcedureOK(){
        Test.startTest();
        Map<String,String> params = TestDataFactory.getTramiteParams(true, true,true);
        Date expDate = Date.today().addYears(4);
        Boolean result = dcPassportReceptionController.updateProcedure(params.get('contactId'), params.get('tramiteId'), 'I862199', expDate);
        System.assert(result);
        Test.stopTest();
        
    }

    static testMethod void updateProcedureInvalidId(){
        Test.startTest();
        Map<String,String> params = TestDataFactory.getTramiteParams(false, true,true);
        Date expDate = Date.today().addYears(4);
        Boolean result = dcPassportReceptionController.updateProcedure(params.get('contactId'), params.get('tramiteId'), 'I862199', expDate);
        System.assert(!result);
        Test.stopTest();
        
    }

}