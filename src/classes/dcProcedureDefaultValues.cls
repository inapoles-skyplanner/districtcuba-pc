/**
 * @File Name          : dcProcedureDefaultValues.cls
 * @Description        : controller class for dc_createProcedureWithDefaultValues Component 
 * @Author             : Elizabeth Betancourt
 * @Group              : 
 * @Last Modified By   : Elizabeth Betancourt
 * @Last Modified On   : 23/7/2020 1:06:36 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    23/7/2020   Elizabeth Betancourt     Initial Version
**/
public class dcProcedureDefaultValues {
   
    @AuraEnabled
    public static dcDefaultValue getDefaults(String contactId, String procConfigId, String contactFields, String configFields) {
        
        string query ='SELECT '+ contactFields + ' FROM Contact WHERE Id = \'' + contactId + '\'' ;
        System.Debug(contactId);
        System.Debug(query);
        List<Contact> contactList = Database.query(query);
        System.Debug(contactList);
        Map<string,string> dvMap1 = new Map<string,string>();
        if (contactList.size() > 0 ){
            dvMap1 = putValuestoDefault(contactFields, contactList[0]);
        }
        
        query ='SELECT '+ configFields + ' FROM Procedure_Setup__c WHERE Id = \'' + procConfigId + '\'' ;
        System.Debug(query);
        List<Procedure_Setup__c> configList = Database.query(query);
        System.Debug(configList);
        Map<string,string> dvMap2 = new Map<string,string>();
        if (configList.size() > 0 ){
            dvMap2 = putValuestoDefault(configFields, configList[0]);
        }
        Map<string,string> dvMap = new  Map<string,string>();
        
        dvMap.putAll(dvMap1);
        dvMap.putAll(dvMap2); 
        
        System.Debug(dvMap);
        
        return new dcDefaultValue(dvMap);
    }
   
    private static Map<string,string> putValuestoDefault(string fields, sObject obj){
        Map<string,string> dvMap = new Map<string,string>();
        
        Map<string, Object> dvMapObject = obj.getPopulatedFieldsAsMap();
        
        List<String> strList = fields.split(',');
        
        for (string key: strList){
            if (dvMapObject.containsKey(key)){
                
                string value = String.valueOf(dvMapObject.get(key));
                System.Debug(value);
                System.Debug(key+':'+value);
                if (value.contains('00:00:00'))
                    value = value.replace('00:00:00', '').trim();
                
                dvMap.put(key,value);   
            }
            else{
                dvMap.put(key,'');
            }                
        }
        return dvMap;
    }
}