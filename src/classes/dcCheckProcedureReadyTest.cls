/**
 * @File Name          : dcCheckProcedureReady.cls
 * @Description        : Class with invocable method to check if required fields have values
 * @Author             : Elizabeth Betancourt Herrera
 * @Group              : 
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 17/8/2020 5:56:07 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/8/2020   Elizabeth Betancourt Herrera     Initial Version
**/
@isTest
public with sharing class dcCheckProcedureReadyTest {
    
      
    @testSetup 
    static void createDataSetupTest(){
        
        Map<String, String> param = new Map<String, String>();
        Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista');
        insert acc;
                
        Contact c = new Contact(FirstName='Luis',LastName='Suarez',AccountId=acc.Id,Eyes_Color__c='Negros');
        insert c;
        
        Date d = date.today();
        //two records for each RecordType, one complete and one incomplete
        List<Procedure__c> procList = new List<Procedure__c>();
        //pasaporte 1ra vez incompleto
        Procedure__c p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId='0123h000000DnH8AAK',
                                          First_Name__c='Luis', Last_Name__c='Suarez', Street__c='calle 10 entre 1ra y 3ra',
                                          Eyes_Color__c='Negros');
        procList.add(p);
        //pasaporte 1ra vez completo
        p = new Procedure__c( Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId='0123h000000DnH8AAK',
                              First_Name__c='Luis', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                              Residence_Country__c='US',State__c='FL', City__c='Miami', Postal_Code__c='33501', 
                              Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                              Departure_Date__c=d, Profession__c='Recepcionista',
							  Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
					          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',
                              Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',
							  Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
						      Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
							  References_Province__c='Artemisa',References_Municipality__c='Artemisa',
                              Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                              Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                              Residence_Address_1__c='ave. Principal entre Marti y Maceo',
                              Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                              Residence_Address_1_From__c = d.addDays(-7300),Residence_Address_1_To__c = d.addDays(-3650));
        procList.add(p);
        //renovacion de pasaporte incompleto
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId='0123h000000DnHNAA0',Numero_de_Pasaporte__c='11111',
                                          First_Name__c='Luis', Last_Name__c='Suarez', Street__c='calle 10 entre 1ra y 3ra',
                                          Eyes_Color__c='Negros');
        procList.add(p);
        //renovacion de pasaporte completo
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId='0123h000000DnHNAA0',Numero_de_Pasaporte__c='11111',
                              First_Name__c='Luis', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                              Residence_Country__c='US',State__c='FL', City__c='Miami', Postal_Code__c='33501',
                              Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                              Departure_Date__c=d, Profession__c='Recepcionista',
							  Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
					          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',Expired_Passport_Issue_Date__c=d,
                              Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',Razon_No_Disponibilidad__c='Perdida',
							  Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
						      Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
							  References_Province__c='Artemisa',References_Municipality__c='Artemisa',
                              Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                              Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                              Residence_Address_1__c='ave. Principal entre Marti y Maceo',
                              Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                              Residence_Address_1_From__c = d.addDays(-7300),Residence_Address_1_To__c = d.addDays(-3650));
        procList.add(p);
        //prorroga incompleto
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId='0123h000000DnHDAA0',Numero_de_Pasaporte__c='11111',
                                          First_Name__c='Luis', Last_Name__c='Suarez', Street__c='calle 10 entre 1ra y 3ra',
                                          Eyes_Color__c='Negros');
        procList.add(p);
        //prorroga completo
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId='0123h000000DnHDAA0',Numero_de_Pasaporte__c='11111',
                              First_Name__c='Luis', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                              Residence_Country__c='US', State__c='FL', City__c='Miami', Postal_Code__c='33501', 
                              Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                              Departure_Date__c=d, Profession__c='Recepcionista',
							  Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
					          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',
                              Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',
							  Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
						      Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
							  References_Province__c='Artemisa',References_Municipality__c='Artemisa',
                              Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                              Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                              Residence_Address_1__c='ave. Principal entre Marti y Maceo',
                              Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                              Residence_Address_1_From__c = d.addDays(-7300),Residence_Address_1_To__c = d.addDays(-3650));
        procList.add(p);
        //prorroga estancia incompleto
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId='0123h0000015OAkAAM',Numero_de_Pasaporte__c='11111',
                                          First_Name__c='Luis', Last_Name__c='Suarez', Street__c='calle 10 entre 1ra y 3ra',
                                          Eyes_Color__c='Negros');
        procList.add(p);
        //prorroga estancia completo
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId='0123h0000015OAkAAM',Numero_de_Pasaporte__c='11111',
                              First_Name__c='Luis', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                              Residence_Country__c='US', State__c='FL', City__c='Miami', Postal_Code__c='33501', 
                              Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                              Departure_Date__c=d, Profession__c='Recepcionista', Prorogue_Month__c=3,
							  Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
					          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',
                              Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',
							  Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
						      Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
							  References_Province__c='Artemisa',References_Municipality__c='Artemisa',
                              Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                              Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                              Residence_Address_1__c='ave. Principal entre Marti y Maceo',
                              Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                              Residence_Address_1_From__c = d.addDays(-7300),Residence_Address_1_To__c = d.addDays(-3650));
        procList.add(p);
        //visa HE-11 incompleto
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId='0123h000000DnHhAAK',Numero_de_Pasaporte__c='11111',
                                          First_Name__c='Luis', Last_Name__c='Suarez', Street__c='calle 10 entre 1ra y 3ra',
                                          Eyes_Color__c='Negros');
        procList.add(p);
        //visa HE-11 completo
        p = new Procedure__c(Agency__c=acc.Id, Customer__c=c.Id, RecordTypeId='0123h000000DnHhAAK',Numero_de_Pasaporte__c='11111',
                              First_Name__c='Luis', Last_Name__c='Suarez', Second_Last_Name__c='Valdés', Street__c='calle 10 entre 1ra y 3ra',
                              Residence_Country__c='US', State__c='FL', City__c='Miami', Postal_Code__c='33501', 
                              Work_Name__c='Costco Call Center', Phone__c='+15657676', Title__c='Recepcionista',
                              Departure_Date__c=d, Profession__c='Recepcionista',
							  Immigration_Status__c='Salida Ilegal', ProfessionCategory__c='Otra',Nivel_de_Escolaridad__c='Primario',
					          Mother_Name__c='Estrella',Father_Name__c='Jose Luis',
                              Birth_Province__c='Artemisa',Birth_Municipality_City__c='Artemisa',
							  Eyes_Color__c='Negros',Hair_Color__c='Negro',Skin_Color__c='Negra',Height__c=172,
						      Reference_Full_Name__c='Ana Margarita',Reference_Second_Surname__c='Alfonso',
							  References_Province__c='Artemisa',References_Municipality__c='Artemisa',
                              Birth_Certificate_Issue_At__c='La Habana', Birth_Certificate_Issue_Date__c= d.addDays(-7500),
                              Reference_Address__c='Calle 50 entre Marti y Maceo',Reference_Last_Name__c='Hurtado',
                              Residence_Address_1__c='ave. Principal entre Marti y Maceo',
                              Residence_Address_1_Province__c='Artemisa',Residence_Address_1_City__c='Artemisa',
                              Residence_Address_1_From__c = d.addDays(-7300),Residence_Address_1_To__c = d.addDays(-3650));
        procList.add(p);
        
        insert procList;
    }
    
    @isTest
    static void testCheckProcedureReady(){
        List<Procedure__c> procList = [SELECT Id FROM Procedure__c];
        
        integer countComplete = 0;
        integer countIncomplete = 0;
        List<String> fields;
        for(Procedure__c proc: procList){
            List<String> idList = new List<String>{proc.Id};
            fields = dcCheckProcedureReady.RequiredEmptyFields(idList);
            if (String.isEmpty(fields[0])) countComplete++;
            else countIncomplete++;
        }
        System.debug(countComplete);
        System.debug(countIncomplete);
        System.assert(countComplete==5);
        System.assert(countIncomplete==5);

    }
}