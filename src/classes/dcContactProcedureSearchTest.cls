/**
 * @File Name          : dcContactProcedureSearchTest.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 31/7/2020 5:25:25 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcContactProcedureSearchTest {

    @isTest
    static void test(){
        Test.startTest();
        new dcContactProcedureSearch();
        Test.stopTest();
        System.assert(true);
    }
}