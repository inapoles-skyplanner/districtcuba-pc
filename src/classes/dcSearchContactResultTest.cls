/**
 * @File Name          : dcSearchContactResultTest.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 31/7/2020 5:19:36 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcSearchContactResultTest {
    @isTest
    static void constructorTest1(){
        Test.startTest();
        dcSearchContactResult result = new dcSearchContactResult(new dcSearchContact());
        Test.stopTest();
        System.assert(result != null);
    }

    @isTest
    static void constructorTest2(){
        Test.startTest();
        List<dcContactProcedureSearch> listResults = new List<dcContactProcedureSearch>();
        dcSearchContactResult result = new dcSearchContactResult(listResults,new dcSearchContact());
        Test.stopTest();
        System.assert(result != null);
    }
}