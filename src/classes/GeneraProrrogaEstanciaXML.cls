/**
 * @File Name          : GeneraProrrogaEstanciaXML.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 17/8/2020 10:44:31 a. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    14/8/2020   Silvia Velazquez     Initial Version
**/
public with sharing class GeneraProrrogaEstanciaXML {
    @InvocableMethod(label='Create Prorroga Estancia XML' description='Create a XML with the information of consular procedures corresponding to the specified Manifest IDs.' category='Procedure__c')
    public static void generaXML(List<String> manifiestos) 
    {       
               
    List<Procedure__c> procedureList = [ SELECT id, Name, RecordType.Name, First_Name__c, Middle_Name__c, Last_Name__c, Second_Last_Name__c, Gender__c, 
                                        Birthday__c, Father_Name__c, Mother_Name__c, Numero_de_Pasaporte__c, Skin_Color__c, Eyes_Color__c, Hair_Color__c, 
                                        Height__c, Profession__c, ProfessionCategory__c, Nivel_de_Escolaridad__c, Title__c, Departure_Date__c, 
                                        Immigration_Status__c, Birth_Country__c, Birth_Province__c, Birth_Municipality_City__c, State__c, City__c, 
                                        Phone__c, Fax__c, Email__c, Residence_Country__c, Postal_Code__c, Street__c, Work_State__c, 
                                        Expired_Passport_Issue_Date__c, Type__c, Work_City__c, Work_Phone__c, Work_Fax__c, Work_Email__c, Work_Country__c, 
                                        Work_Name__c, Work_Postal_Code__c, Work_Street__c, Birth_Certificate__c, Reference_Full_Name__c, Reference_Address__c,
                                        Reference_Phone__c, Comments__c, Category__c, Reference_Second_Name__c, Reference_Last_Name__c, 
                                        Reference_Second_Surname__c, Reference_Provincia__c, Reference_Municipio__c, Reference_District__c,Exit_Permit_Number__c,
                                        Prorogue_Month__c 
                                        FROM Procedure__c
                                        WHERE (Manifest__c in :manifiestos and RecordType.Name='Prorroga de Estancia') LIMIT 30]; 
    
     Dom.Document doc = new Dom.Document();
     /* String str = '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'; 
     doc.load(str); */
     Dom.Xmlnode rootNode = doc.createRootElement('Tramites', null, null); 
    
     for (Procedure__c eachProcedure : procedureList) {           
        Dom.Xmlnode pasaporteNode = rootNode.addChildElement('PrórrogaEstancia-2033', null, null);         
           
		Dom.Xmlnode vchNombre1 = pasaporteNode.addChildElement('vch_nombre1', null, null);
		String valorVchNombre1 = String.isNotBlank(eachProcedure.First_Name__c) ? eachProcedure.First_Name__c.toUpperCase() : '';
        vchNombre1.addTextNode(valorVchNombre1);        

        Dom.Xmlnode vchNombre2 = pasaporteNode.addChildElement('vch_nombre2', null, null);          
        String valorVchNombre2 = String.isNotBlank(eachProcedure.Middle_Name__c) ? eachProcedure.Middle_Name__c.toUpperCase() : '';
        vchNombre2.addTextNode(valorVchNombre2); 

        Dom.Xmlnode vchApellido1 = pasaporteNode.addChildElement('vch_apellido1', null, null);
        String valorVchApellido1 = String.isNotBlank(eachProcedure.Last_Name__c) ? eachProcedure.Last_Name__c.toUpperCase() : '';
        vchApellido1.addTextNode(valorVchApellido1);  

        Dom.Xmlnode vchApellido2 = pasaporteNode.addChildElement('vch_apellido2', null, null);
        String valorVchApellido2 = String.isNotBlank(eachProcedure.Second_Last_Name__c) ? eachProcedure.Second_Last_Name__c.toUpperCase() : '';
        vchApellido2.addTextNode(valorVchApellido2);

        Dom.Xmlnode vchNombrePadre = pasaporteNode.addChildElement('vch_nombrePadre', null, null);
        String valorVchNombrePadre =  String.isNotBlank(eachProcedure.Father_Name__c) ? eachProcedure.Father_Name__c.toUpperCase() : '';
        vchNombrePadre.addTextNode(valorVchNombrePadre);

        Dom.Xmlnode vchNombreMadre = pasaporteNode.addChildElement('vch_nombreMadre', null, null);
        String valorVchNombreMadre = String.isNotBlank(eachProcedure.Mother_Name__c) ? eachProcedure.Mother_Name__c.toUpperCase() : '';
        vchNombreMadre.addTextNode(valorVchNombreMadre);

        Dom.Xmlnode bitSexo = pasaporteNode.addChildElement('bit_sexo', null, null);
        Map<String, String> sexo = new Map<String, String>();  
        sexo.put('Masculino', 'True');
        sexo.put('Femenino', 'False');
        String valorSexo = sexo.containsKey(eachProcedure.Gender__c) ? sexo.get(eachProcedure.Gender__c).toUpperCase() : '';                
        bitSexo.addTextNode(valorSexo);
        
        Dom.Xmlnode dtmFnacimiento = pasaporteNode.addChildElement('dtm_fNacimiento', null, null);
        String fecha = (eachProcedure.Birthday__c!=null) ? XMLresources.formateaFecha(eachProcedure.Birthday__c) : '';
        dtmFnacimiento.addTextNode(fecha);
    
        Dom.Xmlnode vchPasaporte = pasaporteNode.addChildElement('vch_pasaporte', null, null);
        string valorVchPasaporte = String.isNotBlank(eachProcedure.Numero_de_Pasaporte__c) ? eachProcedure.Numero_de_Pasaporte__c.toUpperCase() : '';
        vchPasaporte.addTextNode(valorVchPasaporte);
        
        Dom.Xmlnode bitCubext = pasaporteNode.addChildElement('bit_cubext', null, null);
        bitCubext.addTextNode('');
        
        Dom.Xmlnode dirextIntEstado = pasaporteNode.addChildElement('dirext_int_estado', null, null);
        String valorDirextIntEstado = String.isNotBlank(eachProcedure.State__c) ? eachProcedure.State__c.toUpperCase() : '';
        dirextIntEstado.addTextNode(valorDirextIntEstado); 
        
        Dom.Xmlnode dirextIntCiudad = pasaporteNode.addChildElement('dirext_int_ciudad', null, null);
        String valorDirextIntCiudad= String.isNotBlank(eachProcedure.City__c) ? eachProcedure.City__c.toUpperCase() : '';
        dirextIntCiudad.addTextNode(valorDirextIntCiudad);

        Dom.Xmlnode dirextVchTelefono = pasaporteNode.addChildElement('dirext_vch_telefono', null, null);
        String valorTelefono = String.isNotBlank(eachProcedure.Phone__c) ? eachProcedure.Phone__c : '';
        dirextVchTelefono.addTextNode(valorTelefono);
        
        Dom.Xmlnode dirextVchFax = pasaporteNode.addChildElement('dirext_vch_fax', null, null);
        String valorFax = String.isNotBlank(eachProcedure.Fax__c) ? eachProcedure.Fax__c : '';
        dirextVchFax.addTextNode(valorFax);

        Dom.Xmlnode dirextVchEmail = pasaporteNode.addChildElement('dirext_vch_email', null, null);
        String valorDirextVchEmail = String.isNotBlank(eachProcedure.Email__c) ? eachProcedure.Email__c : '';
        dirextVchEmail.addTextNode(valorDirextVchEmail);

        Dom.Xmlnode dirextIntPais = pasaporteNode.addChildElement('dirext_int_pais', null, null);
        String valordirextIntPais= (eachProcedure.Residence_Country__c!=null) ? String.valueOf(XMLresources.mapPais(eachProcedure.Residence_Country__c)) :'';
        dirextIntPais.addTextNode(valordirextIntPais);

        Dom.Xmlnode dirextIntCodPostal = pasaporteNode.addChildElement('dirext_int_codPostal', null, null);
        String valorPostal = String.isNotBlank(eachProcedure.Postal_Code__c) ? eachProcedure.Postal_Code__c : '';
        dirextIntCodPostal.addTextNode(valorPostal);   

        Dom.Xmlnode dirextVchDireccion = pasaporteNode.addChildElement('dirext_vch_direccion', null, null);
        String valordirextVchDireccion = String.isNotBlank(eachProcedure.Street__c) ? eachProcedure.Street__c.toUpperCase() : '';
        dirextVchDireccion.addTextNode(valordirextVchDireccion);

        Dom.Xmlnode dirVchNropermiso = pasaporteNode.addChildElement('vch_nropermiso', null, null);
        String valordirVchNropermiso = String.isNotBlank(eachProcedure.Exit_Permit_Number__c) ? eachProcedure.Exit_Permit_Number__c.toUpperCase() : '';
        dirVchNropermiso.addTextNode(valordirVchNropermiso);

        Dom.Xmlnode intCantprorroga = pasaporteNode.addChildElement('int_cantprorroga', null, null);
        String valorCantprorroga= (eachProcedure.Prorogue_Month__c != null) ? String.valueOf(eachProcedure.Prorogue_Month__c) : '';
        intCantprorroga.addTextNode(valorCantprorroga);

        Dom.Xmlnode dtmFsalida = pasaporteNode.addChildElement('dtm_fSalida', null, null);
        String strFSalida = (eachProcedure.Departure_Date__c !=null) ? XMLresources.formateaFecha(eachProcedure.Departure_Date__c) : '';
        dtmFsalida.addTextNode(strFSalida);

        Dom.Xmlnode vchObservaciones = pasaporteNode.addChildElement('vch_observaciones', null, null);
        String valorvchObservaciones = String.isNotBlank(eachProcedure.Comments__c) ? eachProcedure.Comments__c.toUpperCase() : '';
        vchObservaciones.addTextNode(valorvchObservaciones);
        
        Dom.Xmlnode bitTrnps = pasaporteNode.addChildElement('bit_trnps', null, null);
        bitTrnps.addTextNode('');

        Dom.Xmlnode vchCodservagencia = pasaporteNode.addChildElement('vch_codservagencia', null, null);            
        String procedureName = String.isNotBlank(eachProcedure.Name) ? 'p'+ XMLresources.procedureNumber(eachProcedure.Name) : '';
        vchCodservagencia.addTextNode(procedureName);
        
        Dom.Xmlnode intRecibidopor = pasaporteNode.addChildElement('int_recibidopor', null, null);
        intRecibidopor.addTextNode('32537');

        Dom.Xmlnode intCorreoenvio = pasaporteNode.addChildElement('int_correoenvio', null, null);
        intCorreoenvio.addTextNode('32537');        
    }   
        
     
     String xmlString = doc.toXmlString();
     xmlString = xmlString.replace('<?xml version="1.0" encoding="UTF-8"?>', '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>');
     System.debug('xmlString =' + xmlString);
      
     XMLresources.addFileXML(manifiestos, xmlString);                     
     
    }
}