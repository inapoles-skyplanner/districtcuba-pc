/**
 * @File Name          : SP_RecordType_ControllerTest.cls
 * @Description        : 
 * @Author             : Ibrahim Napoles
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 7/22/2020, 4:48:04 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/22/2020   Ibrahim Napoles     Initial Version
**/
@isTest
private class SP_RecordType_ControllerTest {
    @IsTest
    static void methodName(){
        Test.startTest();

        Id rtNames = SP_RecordType_Controller.getRecTypeIdByDevName('Procedure__c', 'Pasaporte_1ra_Vez');

        System.assert (rtNames != null);

        Test.stopTest();
    }
}