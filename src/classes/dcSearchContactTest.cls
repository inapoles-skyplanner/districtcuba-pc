/**
 * @File Name          : dcSearchContactTest.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 31/7/2020 5:13:10 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcSearchContactTest {
    @isTest
    static void constructorTest1(){
        Test.startTest();
        dcSearchContact search = new dcSearchContact();
        Test.stopTest();
        System.assert(search != null);
    }

    @isTest
    static void constructorTest2(){
        Test.startTest();
        dcSearchContact search = new dcSearchContact(new dcSearchContact());
        Test.stopTest();
        System.assert(search != null);
    }    
}