/**
 * @File Name          : NumberTOWordConvertionTest.cls
 * @Description        : 
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 11/8/2020 3:53:13 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/8/2020   Silvia Velazquez     Initial Version
**/

@isTest
private class NumberTOWordConvertionTest {
    static Integer intNumber = 40;
    static Integer intNumberZero = 0;
    static Decimal decNumber = 150.05;
    static Decimal decNumberFifty = 150.5;
    static Decimal decNumberZero = 0.5;

    @isTest
    static void CurrencyToWordsFormatInteger(){
        String result = NumberTOWordConvertion.CurrencyToWordsFormat(intNumber);
        System.assertEquals('Forty and 00/100', result);
    }

    @isTest
    static void CurrencyToWordsFormatIntegerZero(){
        String result = NumberTOWordConvertion.CurrencyToWordsFormat(intNumberZero);
        System.assertEquals('Zero and 00/100', result);
    }

    @isTest
    static void CurrencyToWordsFormatDecimal(){
        String result = NumberTOWordConvertion.CurrencyToWordsFormat(decNumber);
        System.assertEquals('One Hundred and Fifty and 5/100', result);
    }

    @isTest
    static void CurrencyToWordsFormatDecimalFifty(){
        String result = NumberTOWordConvertion.CurrencyToWordsFormat(decNumberFifty);
        System.assertEquals('One Hundred and Fifty and 50/100', result);
    }

    @isTest
    static void CurrencyToWordsFormatDecimalZero(){
        String result = NumberTOWordConvertion.CurrencyToWordsFormat(decNumberZero);
        System.assertEquals('Zero and 50/100', result);
    }    
}