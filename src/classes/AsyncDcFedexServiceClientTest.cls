/**
 * @File Name          : AsyncDcFedexServiceClientTest.cls
 * @Description        : Class to test AsyncDcFedexServiceClient class
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 24/7/2020 12:40:56 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    23/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class AsyncDcFedexServiceClientTest {

    @isTest
    static void generateAsyncShipment() {

        AsyncDcShipmentManager asyncShipment = new AsyncDcShipmentManager();
        // Invoke the continuation by calling the action method
        Map<String,Object> params = TestDataFactory.generateShipmentInfo(true, true);
        Continuation conti = asyncShipment.startRequest(params);
        // Verify that the continuation has the proper requests
        Map<String, HttpRequest> requests = conti.getRequests();
        System.assertEquals(requests.size(), 1);
        // Perform mock callout 
        // (i.e. skip the callout and call the callback method)
        HttpResponse response = new HttpResponse();
        response.setBody(TestDataFactory.generateXMLResponse());

        // Set the fake response for the continuation
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);

        // Invoke callback method
        Object result = Test.invokeContinuationMethod(asyncShipment, conti);
        System.debug(asyncShipment);

        // result is the return value of the callback
        System.assertEquals(null, result);

        // Verify that the controller's result variable
        //   is set to the mock response.
        System.assertEquals('ERROR', asyncShipment.result.HighestSeverity);
    }
}