/**
 * @File Name          : SP_RecordType_Controller.cls
 * @Description        : 
 * @Author             : Ibrahim Napoles
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 7/22/2020, 4:43:32 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/22/2020   Ibrahim Napoles     Initial Version
**/
public without sharing class SP_RecordType_Controller {
    public static Id getRecTypeIdByDevName(String objName, String recTypeName) {
        return ((SObject)Type.forName(objName).newInstance())
            .getSObjectType()
            .getDescribe()
            .getRecordTypeInfosByDeveloperName()
            .get(recTypeName)
            .getRecordTypeId();
    }
}