/**
*   @author Michel Carrillo
*   @date   07/18/2014
*   @description This class exposes the SignatureManager funcionality
*/
/***************************************************************************************************************************
Copyright © 2014 SkyPlanner LLC
****************************************************************************************************************************/

public with sharing class dcDigitalSignatureCtrl {
    
    public string sign                  {get;set;}
    private string recordId            	{get;set;}
    private string objectName			{get;set;}
    private string nurl                 {get;set;}
    private string fileName             {get;set;}
    private string attFieldName			{get;set;}

    /*
    *   @author Michel Carrillo
    *   @date   07/18/2014
    *   @description Constructor
    */
    public dcDigitalSignatureCtrl(){
        recordId    	= Apexpages.currentPage().getParameters().get('recordId');
       	objectName		= Apexpages.currentPage().getParameters().get('obn');
        attFieldName	= Apexpages.currentPage().getParameters().get('afn');
        nurl        	= Apexpages.currentPage().getParameters().get('nurl');
        fileName    	= Apexpages.currentPage().getParameters().get('fln');
    }
    
    /*
    *   @author Michel Carrillo
    *   @date   07/18/2014
    *   @description Save signature as attachment of a booking
    */
    public void saveSignature(){
        PageReference pdf       = new PageReference(nurl);
        Attachment attachSign   = new Attachment();
        pdf.getParameters().put('recordId', recordId);
        Blob body;
        
        deleteAttachmentByName(recordId, 'SIGNATURE.png');
        String[] parts = sign.split(',');    
        attachSign.Body = Encodingutil.base64Decode(parts[1]);
        attachSign.Name = 'SIGNATURE.png';
        attachSign.ParentId = recordId;
        insert attachSign;
        
        saveSignature(recordId,objectName, attFieldName, attachSign.Id);
    }
    
    /*
    *   @author Jorge L Fernandez
    *   @date   06/22/2020
    *   @description Store the signature attachment id on a spcecific field of the parent record.
    */
    private void saveSignature(String recordId, String objectName, String signatureAttachmentFieldName, String signatureAttachmentId){
        
        Map<String, Schema.SObjectType> globalDescribe 	= Schema.getGlobalDescribe();
        Schema.SObjectType  objectType					= globalDescribe.get(objectName);
        
        if(objectType != null){
            SObject record = objectType.newSObject(recordId);
            record.put(signatureAttachmentFieldName, signatureAttachmentId);
            update record;
        }
    }
    
    /*
    *   @author Michel Carrillo
    *   @date   07/18/2014
    *   @description Generate a pdf and save it as attachment of the record.
    */
    public PageReference generatePDF(){
        Blob body;
        PageReference pdf       = new PageReference(nurl);
        pdf.getParameters().put('bid', recordId);
        deleteAttachmentByName(recordId, fileName+'.pdf');
        try{
            body = pdf.getContent();
        }
        catch(VisualforceException e){
            body = Blob.valueOf('An error have ocurred. Details: ' + e.getMessage() + '[' + e.getStackTraceString() + ']');
        }
        
        Attachment attachPdf    = new Attachment();
        attachPdf.Body          = body;
        attachPdf.Name          = fileName + '.pdf';
        attachPdf.ParentId      = recordId;
        attachPdf.ContentType   = 'application/pdf';
        insert attachPdf;
        return pdf;
    }
    
    
    /*
    *   @author Michel Carrillo
    *   @date   07/18/2014
    *   @description ---
    */
    private void deleteAttachmentByName(string recordId, string fileName){
        System.debug('ownerId + fileName ===>' + recordId +'+'+ fileName);
        delete [SELECT ParentId, Name, Id, ContentType FROM Attachment WHERE ParentId =: recordId AND Name =: fileName];     
    }
}